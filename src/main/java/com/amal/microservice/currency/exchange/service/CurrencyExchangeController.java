package com.amal.microservice.currency.exchange.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
public class CurrencyExchangeController {
    @Autowired
    private Environment environment;

    @Autowired
    private CurrencyExchangeRepository repository;
    private static Logger logger = LoggerFactory.getLogger(CurrencyExchangeController.class);
    @GetMapping("/currency-exchange/from/{from}/to/{to}")
    public CurrencyExchange getExchangeValue(@PathVariable String from, @PathVariable String to)
    {
        logger.info("GET currency exchange called with FROM: {} and TO: {}",from,to);
        //CurrencyExchange currencyExchange = new CurrencyExchange(1000L, from, to, BigDecimal.valueOf(65L));
        CurrencyExchange currencyExchange = repository.findByFromAndTo(from, to);
        if (currencyExchange==null)
            throw new RuntimeException("Unable to find conversion factor for from:"+from+", to:"+to);
        currencyExchange.setEnvironment(environment.getProperty("local.server.port"));
        return currencyExchange;
    }
}
