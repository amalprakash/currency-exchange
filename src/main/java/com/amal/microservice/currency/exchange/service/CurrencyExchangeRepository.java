package com.amal.microservice.currency.exchange.service;

import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigDecimal;

public interface CurrencyExchangeRepository extends JpaRepository<CurrencyExchange,Long> {
    public CurrencyExchange findByFromAndTo(String from,String to);

    }
